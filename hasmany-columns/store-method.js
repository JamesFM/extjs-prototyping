Ext.onReady(function() {

	// models
	Ext.define('Daughter', {
		extend: 'Ext.data.Model',
		belongsTo: 'Father',
		fields: [ 'age', 'name' ]
	});

	Ext.define('Father', {
		extend: 'Ext.data.Model',
		fields: [ 'id', 'married', 'name', 'sons' ],
		hasMany: 'Daughter'
	});
	
	// store
	Ext.create('Ext.data.Store', {
		storeId: 'fatherStore',
		autoLoad: true,
		model: 'Father',
		proxy: { 
			type: 'ajax',
			url: 'data1.json',
			reader: {
				root: 'fathers'
			}
		},
		listeners: {
			load: function(store, records, successful, eOpts) {
				if (!successful) {
					return;
				}

				var fathers = [];
				var maxDaughters = 0;
				Ext.Array.forEach(records, function(record) {
					var number = record.daughters().data.items.length;
					if (number > maxDaughters) {
						maxDaughters = number;
					}

					var father = record.data; 
					for (var i=0; i < number; i++) {
						father['daughter' + i] = record.daughters().data.items[i].data; 
						fathers.push(father);
					}
				});

				buildGrid(fathers, maxDaughters);
			}
		}
	});

	var buildGrid = function(fathers, maxDaughters) {

		var buildFields = function(maxDaughters) {
			var fields = [ 'id', 'married', 'name', 'sons' ];
			for (var i = 0; i < maxDaughters; i++) {
				fields.push('daughter' + i);
			}
			return fields;
		};

		Ext.create('Ext.data.Store', {
			storeId: 'flattenedStore',
			autoLoad: true,
			fields: buildFields(maxDaughters),
			data: { fathers: fathers },
			proxy: {
				type: 'memory',
				reader: {
					type: 'json',
					root: 'fathers'
				}
			}
		});

		// columns
		var buildColumns = function(maxDaughters) {
			var columns = [
				{
					xtype: 'rownumberer'
				},
				{
					text: 'name',
					dataIndex: 'name'
				},
				{ 
					text: 'married',
					dataIndex: 'married'
				},
				{
					text: 'sons',
					dataIndex: 'sons'
				}
			];
			for (var i = 0; i < maxDaughters; i++) {
				var d = 'daughter' + i;
				columns.push({
					text: d,
					columns: [
						{
							text: 'name',
							xtype: 'templatecolumn',
							tpl: '{' + d + '.name}'
						},
						{
							text: 'age',
							xtype: 'templatecolumn',
							tpl: '{' + d + '.age}'
						}
					]
				});
			}
			return columns;
		};

		// grid panel
		Ext.create('Ext.grid.Panel', {
			id: 'gridpanel',
			title: 'Fathers',
			renderTo: Ext.getBody(),
			width: 600,
			height: 400,
			store: Ext.data.StoreManager.lookup('flattenedStore'),
			columns: buildColumns(maxDaughters)
		});
	};
});
