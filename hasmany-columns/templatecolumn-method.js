Ext.onReady(function() {

	// models
	Ext.define('Daughter', {
		extend: 'Ext.data.Model',
		belongsTo: 'Father',
		fields: [ 'age', 'name' ]
	});

	Ext.define('Father', {
		extend: 'Ext.data.Model',
		fields: [ 'id', 'married', 'name', 'sons' ],
		hasMany: 'Daughter'
	});
	
	// store
	Ext.create('Ext.data.Store', {
		storeId: 'fatherStore',
		autoLoad: true,
		model: 'Father',
		proxy: { 
			type: 'ajax',
			url: 'data1.json',
			reader: {
				root: 'fathers'
			}
		},
		listeners: {
			load: function(store, records, successful, eOpts) {
				if (!successful) {
					return;
				}

				var maxDaughters = 0;
				Ext.Array.forEach(records, function(record) {
					var number = record.daughters().data.items.length;
					if (number > maxDaughters) {
						maxDaughters = number;
					}
				});

				buildGrid(maxDaughters);
			}
		}
	});

	var buildGrid = function(maxDaughters) {

		// columns
		var buildColumns = function(maxDaughters) {
			var columns = [
				{
					xtype: 'rownumberer'
				},
				{
					text: 'name',
					dataIndex: 'name'
				},
				{ 
					text: 'married',
					dataIndex: 'married'
				},
				{
					text: 'sons',
					dataIndex: 'sons'
				}
			];
			for (var i = 0; i < maxDaughters; i++) {
				columns.push({
					text: 'daughter' + i,
					columns: [
						{
							text: 'name',
							xtype: 'templatecolumn',
							tpl: '{[values.daughters[' + i + '] ? values.daughters[' + i + '].name : "" ]}'
						},
						{
							text: 'age',
							xtype: 'templatecolumn',
							tpl: '{[values.daughters[' + i + '] ? values.daughters[' + i + '].age : "" ]}'
						}
					]
				});
			}
			return columns;
		};

		// grid panel
		Ext.create('Ext.grid.Panel', {
			id: 'gridpanel',
			title: 'Fathers',
			renderTo: Ext.getBody(),
			width: 600,
			height: 400,
			store: Ext.data.StoreManager.lookup('fatherStore'),
			columns: buildColumns(maxDaughters)
		});
	};

});
