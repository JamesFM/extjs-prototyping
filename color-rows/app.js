Ext.onReady(function() {
	Ext.create('Ext.data.Store', {
		storeId:'simpsonsStore',
		fields:['name', 'email', 'change'],
		data:{'items':[
			{ 'name': 'Lisa',  "email":"lisa@simpsons.com",  "change":100  },
			{ 'name': 'Bart', "email":"bart@simpsons.com", "change":-20  },
			{ 'name': 'Homer', "email":"home@simpsons.com",  "change":23   },
			{ 'name': 'Marge', "email":"marge@simpsons.com", "change":-11   }
		]},
		proxy: {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	Ext.create('Ext.grid.Panel', {
		title: 'Simpsons',
		store: Ext.data.StoreManager.lookup('simpsonsStore'),
		columns: [
			{ header: 'Name',  dataIndex: 'name' },
			{ header: 'Email', dataIndex: 'email', flex: 1 },
			{ header: 'Change', dataIndex: 'change', tdCls: 'x-change-cell' }
		],
		height: 200,
		width: 400,
		viewConfig: {
			stripeRows: false,
			getRowClass: function(record, index) {
				var c = record.get('change');
				if (c < 0) {
					return 'price-fall';
				} else if (c > 0) {
					return 'price-rise';
				}
			}
		},
		renderTo: Ext.getBody()
	});
});
