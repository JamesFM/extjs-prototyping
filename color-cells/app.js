Ext.onReady(function() {

	// model
	Ext.define('ColorSet', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'id' },
			{ name: 'color1' },
			{ name: 'color2' },
			{ name: 'color3' }
		]
	});

	// store
	Ext.create('Ext.data.Store', {
		storeId: 'colorSetStore',
		autoLoad: true,
		model: 'ColorSet',
		proxy: {
			type: 'ajax',
			url: 'data.json',
			reader: {
				root: 'colorsets'
			}
		}
	});

	// renderer for color cells
	var colorRenderer = function(value) {
		return '<span style="background-color:' + value + ';">' + value + '</span>';
	};

	// columns
	var columns = {
		defaults: {
			flex: 1,
			renderer: colorRenderer,
			style: {
				// why is this only effecting the header?
				'font-weight': 'bold'
			}
		},
		items: [
			{
				xtype: 'rownumberer'
			},
			{
				text: 'color1',
				dataIndex: 'color1'
			},
			{
				text: 'color2',
				dataIndex: 'color2'
			},
			{
				text: 'color3',
				dataIndex: 'color3'
			}
		]
	};

	// grid panel
	Ext.create('Ext.grid.Panel', {
		title: 'Colors',
		renderTo: Ext.getBody(),
		width: 600,
		height: 400,
		store: Ext.data.StoreManager.lookup('colorSetStore'),
		columns: columns
	});
});
