# simple converter to convert workbench csv export by column to json
import sys, csv, json

group_name = "Simulations"
file_path = sys.argv[1]
# rb = read binary
with open(file_path, 'rb') as f:
    input = csv.reader(f, delimiter=',')
    
    # first row is simulation name headers
    noSims = []
    simDict = {}
    for i, col in enumerate(input.next()):
        if col is None or len(col) == 0:
            noSims.append(i)
            continue
        if not simDict.has_key(col):
            simDict[col] = [i]
        else:
            simDict[col].append(i)

    # column names with removed spaces
    colNames = [i.replace(' ','') for i in input.next()]

    rows = []
    for r in input:
        rDic = {}

        for i in noSims:
            rDic[colNames[i]] = r[i]

        # columns in sims
        mDic = {}
        for key in simDict:
            if not mDic.has_key(key):
                mDic[key] = {}
            for val in simDict[key]:
                mDic[key][colNames[val]] = r[val]

        rDic[group_name] = mDic
        rows.append(rDic)

    print json.dumps(rows, sort_keys=True, indent=4)

