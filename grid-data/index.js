Ext.require(['Ext.grid.Panel', 'Ext.data.Store']);

Ext.onReady(function() {

	// might not need to extend this, look at memory proxy
	/*
	Ext.define('MockProxy', {
			extend: 'Ext.data.proxy.Proxy',
	});
	*/

	Ext.define('Employee', {
		extend: 'Ext.data.Model',
		fields: ['firstname', 'lastname', {
			name: 'senority',
			type: 'int'
		},
		'dep', {
			name: 'hired',
			type: 'date',
			format: 'm/d/Y'
		}]
	});

	var data = getMockJSON();

	store = Ext.create('Ext.data.Store', {
		storeId: 'employeeStore',
		data: data,
		model: 'Employee',
		proxy: {
			type: 'memory',
			reader: {
				type: "json",
				root: "employees"
			}
		}
	});

	// store.getAt('0').set('lastname', 'JOHNSON');
	Ext.create('Ext.grid.Panel', {
		title: 'Employees',
		store: store,
		columns: [{
			xtype: 'rownumberer',
			width: 35
		},
		{
			text: 'First Name',
			dataIndex: 'firstname'
		},
		{
			text: 'Last Name',
			dataIndex: 'lastname'
			// locked: true
		},
		{
			text: 'Name',
			getSortParam: function() {
				return 'lastname';
			},
			renderer: function(value, meta, record) {
				return record.get('firstname') + ' ' + record.get('lastname');
			}
		},
		{
			xtype: 'datecolumn',
			text: 'Hired Month',
			dataIndex: 'hired',
			format: 'M, y'
		},
		{
			xtype: 'templatecolumn',
			text: 'Deparment (Yrs)',
			tpl: '<b>{dep}</b> ({senority})'
		},
		{
			text: 'Hi',
			renderer: function() {
				return '<b>Hi</b> there!';
			}
		}],
		width: 600,
		height: 400,
		selType: 'rowmodel',
		renderTo: Ext.getBody()
	});

});

