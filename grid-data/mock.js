$.mockJSON(/mock\.json/, {
	"employees|1000-1000": [{
		"firstname": "@MALE_FIRST_NAME",
		"lastname": "@LAST_NAME",
		"senority|1-7": 1,
		"dep": "@LOREM",
		"hired": "@DATE_MM/@DATE_DD/@DATE_YYYY"
	}]
});

var getMockJSON = function() {
	console.log('generating mock json');
	var mockJson = null;

	$.getJSON('mock.json', function(json) {
			mockJson = json;
	});

	console.log('finished generating mock json');
	return mockJson;
};
