Ext.onReady ->
	Ext.create('Ext.form.Panel', 
		title: 'Simple Form'
		bodyPadding: 5
		width: 350

		# the form will submit an AJAX request to this URL when submitted
		url: 'save-form.php'

		# Fields will be arragned vertically, stretched to full width
		layout: 'anchor'
		defaults:
			anchor: '100%'

		# The fields
		defaultType: 'textfield'
		items: [
			fieldLabel: 'First Name'
			name: 'first'
			allowBlank: false
		,
			fieldLabel: 'Last Name'
			name: 'last'
			allowBlank: false
		]

		# Reset and Submit buttons
		buttons: [
			text: 'Reset'
			handler: -> @.up('form').getForm().reset()
		,
			text: 'Submit'
			formBind: true #only enabled once the form is valid
			disabled: true
			handler: ->
				form = @.up('form').getForm()
				if (form.isValid())
					form.submit(
						success: (form, action) -> 
							Ext.Msg.alert('Success', action.result.msg)
						failure: (form, action) -> 
							Ext.Msg.alert('Failed', action.result.msg)
					)
		]

		renderTo: Ext.getBody()
	)
