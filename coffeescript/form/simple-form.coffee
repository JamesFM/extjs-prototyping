Ext.onReady ->
	Ext.QuickTips.init()
	required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>'

	Ext.widget( 
		xtype: 'form'
		layout: 'form'
		collapsible: true
		id: 'simpleForm'
		url: 'save-form.php'
		frame: true
		title: 'Simple Form'
		bodyPadding: '5 5 0'
		width: 350
		fieldDefaults:
			msgTarget: 'side'
			labelWidth: 75
		defaultType: 'textfield'
		items: [
			fieldLabel: 'First Name'
			afterLabelTextTpl: required
			name: 'first'
			allowBlank: false
		,
			fieldLabel: 'Last Name'
			afterLabelTextTpl: required
			name: 'last'
		,
			fieldLabel: 'Company'
			name: 'company'
		,
			fieldLabel: 'Email'
			afterLabelTextTpl: required
			name: 'email'
			vtype: 'email'
		,
			fieldLabel: 'DOB'
			name: 'dob'
			xtype: 'datefield'
		,
			fieldLabel: 'Age'
			name: 'age'
			xtype: 'numberfield'
			minValue: 0
			maxValue: 100
		,
			fieldLabel: 'Time'
			name: 'time'
			xtype: 'timefield'
			minValue: '8:00am'
			maxValue: '6:00pm'
		]

		buttons: [
			text: 'Save'
		,
			text: 'Cancel'
		]

		renderTo: Ext.getBody()
	)
