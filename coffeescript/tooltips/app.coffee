Ext.onReady ->
	# enable tooltips globally
	Ext.QuickTips.init()

	# taken from http://www.sencha.com/forum/showthread.php?205742-Tooltips-on-column-cells
	renderTip = (val, meta, rec, rowIndex, colIndex, store) ->
		meta.tdAttr = 'data-qtip="' + rec.data.tooltip + '"'
		val
	
	Ext.create('Ext.data.Store',
		storeId: 'simpsonsStore'
		fields: ['name', 'email', 'tooltip']
		data:
			items: [
				name: 'Lisa'
				email: 'lisa@simpsons.com'
				tooltip: 'green peace'
			,
				name: 'Bart'
				email: 'bart@simpsons.com'
				tooltip: 'skateboards'
			,
				name: 'Homer'
				email: 'homer@simpsons.com'
				tooltip: 'beer'
			,
				name: 'Marge'
				email: 'marge@simpsons.com'
				tooltip: 'blue hair'
			]
		proxy:
			type: 'memory'
			reader:
				type: 'json'
				root: 'items'
	)

	Ext.create('Ext.grid.Panel',
		title: 'Simpsons'
		renderTo: Ext.getBody()
		store: Ext.data.StoreManager.lookup('simpsonsStore')
		columns:
			defaults:
				renderer: renderTip
			items: [
				header: 'Name'
				dataIndex: 'name'
			,
				header: 'Email'
				dataIndex: 'email'
				flex: 1
			]
		height: 200
		width: 400
	)
