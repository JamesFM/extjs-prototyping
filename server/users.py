from flask import Flask, jsonify, json, request, abort, make_response
from crossdomain import crossdomain
app = Flask(__name__)

users = {
        'data': [
            { 'id': 1, 'first': 'Fred', 'last': 'Flintstone', 'email': 'fred@flintstone.com' },
            { 'id': 2, 'first': 'Wilma', 'last': 'Flintstone', 'email': 'wilma@flintstone.com' },
            { 'id': 3, 'first': 'Pebbles', 'last': 'Flintstone', 'email': 'pebbles@flintstone.com' },
            { 'id': 4, 'first': 'Barney', 'last': 'Rubble', 'email': 'barney@rubble.com' }
        ]
}

@app.route("/")
@crossdomain(origin="*")
def test():
    return jsonify(msg="Flask is running")

@app.route("/users", methods=['OPTIONS', 'GET', 'POST'])
@crossdomain(origin="*")
def users_endpoint():
    print 'users called'

    if request.method == 'GET': 
        return getUsers()
    elif request.method == 'POST':
        print 'POST: ', request
        print 'json', request.json

        new_user = request.json
        new_id = len(users['data'])
        new_user['id'] = new_id
        users['data'].append(new_user)  
        
        return str(new_user)
        #resp = make_response(None, 201)
        #return resp

    abort(404)

@app.route("/users/<int:user_id>", methods=['OPTIONS', 'DELETE', 'PUT'])
@crossdomain(origin="*")
def user_endpoint(user_id):
    print 'change_user called with user_id: ', user_id

    if request.method == 'DELETE':
        print 'delete'
        abort(404)
    elif request.method == 'PUT':
        print 'PUT: ', request
        abort(404)
    abort(404)

def getUsers():
    out = json.dumps(users, sort_keys=False, indent=4)
    #JSONP
    #out = '(' + out + ')'
    # app.logger.debug(out)
    #return out
    # forcing proper mime type here
    return app.response_class(out, mimetype='application/json')


if __name__ == "__main__":
    app.debug = True
    app.run()
