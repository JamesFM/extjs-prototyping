Ext.Loader.setConfig({
    enabled: true
});

Ext.application({
    views: [
        'FileForm'
    ],
    autoCreateViewport: true,
    name: 'FileForm',
    controllers: [
        'FileController'
    ]
});
