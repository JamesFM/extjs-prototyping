Ext.define('FileForm.controller.FileController', {
    extend: 'Ext.app.Controller',

    refs: [
        {
            ref: 'fileForm',
            selector: '#fileForm'
        }
    ],

    onFileFormSubmitClick: function(button, e, options) {
        var form = this.getFileForm().getForm();

        console.log('onFileFormSubmitClick');

        form.submit({
            success: function(form, action) {
                Ext.Msg.alert('Success', action.result.msg);
            },
            failure: function(form, action) {
                switch (action.failureType) {
                    case Ext.form.action.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.action.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.action.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', action.result.msg);
                }
            }
        });
    },

    init: function(application) {
        this.control({
            "#fileFormSubmit": {
                click: this.onFileFormSubmitClick
            }
        });
    }

});
