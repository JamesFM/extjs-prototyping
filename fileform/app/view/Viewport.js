Ext.define('FileForm.view.Viewport', {
    extend: 'FileForm.view.FileForm',
    renderTo: Ext.getBody(),
    requires: [
        'FileForm.view.FileForm'
    ]
});
