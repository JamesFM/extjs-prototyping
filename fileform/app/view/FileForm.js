Ext.define('FileForm.view.FileForm', {
    extend: 'Ext.form.Panel',

    height: 250,
    id: 'fileForm',
    width: 400,
    bodyPadding: 10,
    title: 'File Form',
    url: 'http://localhost:9000/rest/files',

    initComponent: function() {
        var me = this;

        me.initialConfig = Ext.apply({
            url: 'http://localhost:9000/rest/files'
        }, me.initialConfig);

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'filefield',
                    anchor: '100%',
                    id: 'fileUpload',
                    name: 'file',
                    allowBlank: false,
                    emptyText: 'Select a file to upload'
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    layout: {
                        pack: 'end',
                        type: 'hbox'
                    },
                    items: [
                        {
                            xtype: 'button',
                            formBind: true,
                            id: 'fileFormSubmit',
                            text: 'Upload'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});
