Ext.define('Ext.form.Action', {
    extend: 'Ext.form.Basic',

    submit: function(options) {
        var me = this,
            action;

        options = options              || {};
        action  = options.submitAction || me.submitAction;

        if ( action ) {
            return me.doAction(action, options);
        }
        else {
            return me.callParent(arguments);
        }
    },

    load: function(options) {
        var me = this,
            action;

        options = options            || {};
        action  = options.loadAction || me.loadAction;

        if ( action ) {
            return me.doAction(action, options);
        }
        else {
            return me.callParent(arguments);
        }
    }
});
